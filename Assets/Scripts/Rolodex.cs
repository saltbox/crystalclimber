﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.UI;

public class Rolodex : MonoBehaviour {
    public GameObject cardPrefab;
    public AnimationCurve curve;
    public GameObject[] blockPrefabs;
    List<GameObject> cards;
    int selectedCard = 0;
    int displayedSpan = 3;
    int widthMultiplier = 15;
    float selectionInterp;
	// Use this for initialization
	void Start () {
        blockPrefabs = Resources.LoadAll("Prefabs/Blocks/Placeable", typeof(GameObject)).Cast<GameObject>().ToArray();
        cards = new List<GameObject>();
        foreach (GameObject block in blockPrefabs)
        {
            GameObject newCard = GameObject.Instantiate(cardPrefab);
            newCard.transform.SetParent(transform);
            newCard.transform.SetAsFirstSibling();
            newCard.transform.FindChild("Image").GetComponent<Image>().sprite = block.GetComponent<SpriteRenderer>().sprite;
            newCard.transform.FindChild("Panel").FindChild("Text").GetComponent<Text>().text = block.name;
            cards.Add(newCard);
        }
	}

    GameObject GetCard(int index)
    {
        /*
        if (index >= cards.Count)
        {
            index -= cards.Count * Mathf.CeilToInt(index / cards.Count);
        }
        if (index < 0)
        {
            index += cards.Count * Mathf.CeilToInt(index / cards.Count);
        }
         */
        if (index >= cards.Count || index < 0)
        {
            return null;
        }
        return cards[index];
    }

    List<GameObject> GetCardsAdjacentTo(int index, int span)
    {
        List<GameObject> adjacentCards = new List<GameObject>();
        for (int i = index - span; i <= index + span; i++)
        {
            GameObject c = GetCard(i);
            //if (c != null)
            {
                adjacentCards.Add(c);
            }
        }
        return adjacentCards;
    }

	// Update is called once per frame
	void Update () {
        selectionInterp = Mathf.Lerp(selectionInterp, selectedCard, 0.25f);
        if (Mathf.Abs(selectionInterp - selectedCard) < 0.01f)
        {
            selectionInterp = selectedCard;
        }
        List<GameObject> adjacentCards = GetCardsAdjacentTo(Mathf.RoundToInt(selectionInterp), displayedSpan);
        for (int i = 0; i < adjacentCards.Count; i++)
        {
            if (adjacentCards[(adjacentCards.Count - 1) - i] != null)
            {
                RectTransform newCardTransform = adjacentCards[(adjacentCards.Count - 1) - i].GetComponent<RectTransform>();
                newCardTransform.offsetMin = new Vector2((i + wrap01(selectionInterp)) * widthMultiplier, curve.Evaluate(((float)i + wrap01(selectionInterp)) / (float)adjacentCards.Count));
                newCardTransform.offsetMax = new Vector2(-50 + (i + wrap01(selectionInterp)) * widthMultiplier, -50 + curve.Evaluate(((float)i + wrap01(selectionInterp)) / (float)adjacentCards.Count));
            }
        }
        for (int i = 1; i <= 9; i++)
        {
            if (Input.GetButtonDown(i.ToString()) && i <= cards.Count)
            {
                selectedCard = i - 1;
                Debug.Log(selectedCard);
            }
        }
	}


    float wrap01(float f)
    {
       return f - Mathf.Round(f);
    }
}
