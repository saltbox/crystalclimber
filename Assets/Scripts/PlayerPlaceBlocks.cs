﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class PlayerPlaceBlocks : MonoBehaviour {
    public GameObject[] blockPrefabs;
    public int selectedPrefab;
	// Use this for initialization
	void Start () {
        blockPrefabs = Resources.LoadAll("Prefabs/Blocks/Placeable", typeof(GameObject)).Cast<GameObject>().ToArray();
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 1; i <= 9; i++)
        {
            if (Input.GetButtonDown(i.ToString()))
            {
                selectedPrefab = i - 1;
            }
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Vector2 clickPoint = GetRoundedMousePosition();
            if (IsCellEmpty(clickPoint) && HasAdjacentBlock(clickPoint))
            {
                SpawnBlock(blockPrefabs[selectedPrefab], clickPoint);
            }
        }

        if (Input.GetButtonDown("Fire2"))
        {
            Collider2D block = GetBlockAt(GetRoundedMousePosition());
            if (block != null && block.gameObject.GetComponent<Block>() != null)
            {
                GameObject.Destroy(block.gameObject);
            }
        }
	}

    Vector2 GetRoundedMousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition).Rounded() - new Vector3(0, 0, Camera.main.transform.position.z);
    }

    void SpawnBlock(GameObject blockPrefab, Vector2 position)
    {
        GameObject newBlock = GameObject.Instantiate(blockPrefab);
        newBlock.transform.position = position;
        newBlock.GetComponent<Block>().BlockPrefab = blockPrefab;
        StartCoroutine(CheckMouseDrag(newBlock));
    }

    IEnumerator CheckMouseDrag(GameObject spawnedBlock)
    {
        while (true)
        {
            if (!Input.GetButton("Fire1"))
            {
                break;
            }
            if (!MousedOver(spawnedBlock))
            {
                Vector2 p = RelativeMousePosition(spawnedBlock);
                if (p != (Vector2)spawnedBlock.transform.position)
                {

                    SpawnBlock(spawnedBlock.GetComponent<Block>().BlockPrefab, p);
                    break;
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    Vector2 RelativeMousePosition(GameObject block)
    {
        Vector2[] directions = new Vector2[] { Vector2.left, Vector2.right, Vector2.up, Vector2.down };
        foreach (Vector2 v in directions)
        {
            if (GetRoundedMousePosition() == (Vector2)block.transform.position + (Vector2)v)
            {
                return (Vector2)block.transform.position + v;
            }
        }
        return block.transform.position;
    }

    bool MousedOver(GameObject block)
    {
        Collider2D c = Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        return c != null && c.gameObject == block;
    }

    bool HasAdjacentBlock(Vector2 position)
    {
        Collider2D[] vCols = Physics2D.OverlapCircleAll(position, 0.55f, 1 << LayerMask.NameToLayer("Platform"));
        return vCols.Length > 0;
    }

    bool IsCellEmpty(Vector2 pos)
    {
        return GetBlockAt(pos) == null;
    }

    Collider2D GetBlockAt(Vector2 pos)
    {
        return Physics2D.OverlapCircle(pos, 0.25f, 1 << LayerMask.NameToLayer("Platform"));
    }
}
