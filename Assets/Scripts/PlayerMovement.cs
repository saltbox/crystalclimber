﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Prime31;

public class PlayerMovement : MonoBehaviour {
    public bool noclip;
    public Vector2 moveInput;
    public Vector2 velocity;

    public float horizontalRunLimit = 0.1f;
    public float verticalJumpForce = 0.1f;

    CharacterController2D characterController;
    Rigidbody2D rigidBody2D;

	// Use this for initialization
	void Start () {
        moveInput = Vector2.zero;
        characterController = GetComponent<CharacterController2D>();
        rigidBody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        
        moveInput.x = Input.GetAxis("Horizontal");
        if (noclip)
        {
            moveInput.y = Input.GetAxis("Vertical");
        }
        else
        {
            if (Input.GetButtonDown("Jump") && characterController.isGrounded)
            {
                moveInput.y = verticalJumpForce;
                velocity.y = 0;
            }
        }

        if (characterController.collisionState.above)
        {
            velocity.y = 0;
        }
        
	}

    void FixedUpdate()
    {
        velocity.x = Mathf.Lerp(velocity.x, 0, 0.3f);

        if (!characterController.isGrounded)
        {
            velocity += Physics2D.gravity;
        }

        if (characterController.collisionState.becameGroundedThisFrame)
        {
            Debug.Log("became grounded");
        }

        

        velocity += moveInput;

        velocity.x = Mathf.Clamp(velocity.x, -horizontalRunLimit, horizontalRunLimit);
        characterController.move(velocity);
        transform.position = (transform.position * 32).Rounded() / 32;
        moveInput = Vector2.zero;

        
    }

    
    
}
