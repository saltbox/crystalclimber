﻿using UnityEngine;
using System.Collections;

public enum ItemType
{
    Stone,
    Iron,
    Crystal,
    Pipe,
    Ammunition,
    Machine,
    LaserAmmo,
    Turret,
    Stonebot,

    COUNT
}