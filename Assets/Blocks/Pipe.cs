﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pipe : MonoBehaviour {

    private ItemType type;
    public int direction; // The direction the pipe is moving items to

    public const int NORTH = 0;
    public const int EAST = 1;
    public const int SOUTH = 2;
    public const int WEST = 3;

    int outletCount = 0;
    public List<Pipe> outlets = new List<Pipe>();

    public void reInitPipe()
    {
        initPipe(direction);
    }

    public void Start()
    {
        reInitPipe();
    }

    public void initPipe(int direction)
    {
        this.direction = direction;

        Pipe n = null;
        //NORTH
        if (tryGetNeighbors(new Vector2(0, 1), out n))
        {
            if (n.direction == direction)
            {
                outlets.Add(n);
            }
        }
        //EAST
        if (tryGetNeighbors(new Vector2(1, 0), out n))
        {
            if (n.direction == direction)
            {
                outlets.Add(n);
            }
        }
        //WEST
        if (tryGetNeighbors(new Vector2(-1, 0), out n))
        {
            if (n.direction == direction)
            {
                outlets.Add(n);
            }
        }
        //SOUTH
        if (tryGetNeighbors(new Vector2(0, -1), out n))
        {
            if (n.direction == direction)
            {
                outlets.Add(n);
            }
        }

    }

    private bool tryGetNeighbors(Vector2 direction, out Pipe neighbor)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(direction + (Vector2)transform.position, 0.25f);
        for (int i = 0; i < colliders.Length; i++)
        {
            neighbor = colliders[i].GetComponent<Pipe>();
            if (neighbor != null && neighbor != this)
            {
                return true;
            }
        }

        neighbor = null;
        return false;
    }

    public void removeFromMap()
    {
        Pipe n = null;
        //NORTH
        if (tryGetNeighbors(new Vector2(0, 1), out n))
        {
            if(n.outlets.Contains(this))
            {
                n.outlets.Remove(this);
            }
        }
        //EAST
        if (tryGetNeighbors(new Vector2(1, 0), out n))
        {
            if (n.outlets.Contains(this))
            {
                n.outlets.Remove(this);
            }
        }
        //WEST
        if (tryGetNeighbors(new Vector2(-1, 0), out n))
        {
            if (n.outlets.Contains(this))
            {
                n.outlets.Remove(this);
            }
        }
        //SOUTH
        if (tryGetNeighbors(new Vector2(-1, 0), out n))
        {
            if (n.outlets.Contains(this))
            {
                n.outlets.Remove(this);
            }
        }
    }
}
