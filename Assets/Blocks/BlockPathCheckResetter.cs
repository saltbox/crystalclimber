﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockPathCheckResetter : MonoBehaviour {
    public static List<Block> blocks = new List<Block>();
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        foreach (Block b in blocks)
        {
            b.pathedThisFrame = false;
        }
	}
}
