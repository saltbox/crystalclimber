﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Block : MonoBehaviour
{
    public float decayTime = 0.1f;
    public bool isGround, pathedThisFrame;
    public int layer;
    public GameObject BlockPrefab;

    BoxCollider2D _boxCollider2D;

    public const int NORTH = 0;
    public const int EAST = 1;
    public const int SOUTH = 2;
    public const int WEST = 3;
    public Block[] neighbors = new Block[4];

    private bool isFalling;
    private Coroutine decayTimerCoroutine;
    private string blockType = "Brick";

    Vector2 velocity;

    // Use this for initialization
    void Start ()
    {
        //BlockPrefab = PrefabReference
        _boxCollider2D = GetComponent<BoxCollider2D>();
        updateLayer();
        addToMap();
        if (!isGround)
        {
            if (!isGrounded(this))
            {
                forceRecheck();
            }
        }
    }
    
    IEnumerator decayTimer(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        if(!isGrounded(this))
        {
            drop();
        }
        decayTimerCoroutine = null;
    }

    public void forceRecheck()
    {
        if (decayTimerCoroutine != null)
        {
            StopCoroutine(decayTimerCoroutine);
            decayTimerCoroutine = null;
        }
        decayTimerCoroutine = StartCoroutine(decayTimer(decayTime));
    }


    void OnDestroy()
    {
        removeFromMap();
        forceNeighborsRecheck();
    }

    private void updateLayer()
    {
        layer = (int)transform.position.y;
    }

    private void drop()
    {
        removeFromMap();
        forceNeighborsRecheck();
        clearNeighbors();
        //TODO: DROP LOGIC
        StartCoroutine(C_Drop());
        
    }

    IEnumerator C_Drop()
    {
        isFalling = true;
        while (true)
        {
            yield return new WaitForFixedUpdate();
            velocity += Physics2D.gravity;
            RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(0, -0.51f), Vector2.down, Mathf.Abs(velocity.y));
            if (hit.collider != null && hit.collider.gameObject.layer == LayerMask.NameToLayer("Platform") && !hit.collider.GetComponent<Block>().isFalling)
            {
                transform.position = hit.collider.transform.position + Vector3.up;
                isFalling = false;
                addToMap();
                velocity = Vector2.zero;
                break;
            }
            transform.position += (Vector3)velocity;
        }
    }

    private void clearNeighbors()
    {
        for (int i = 0; i < neighbors.Length; i++)
        {
            neighbors[i] = null;
        }
    }

    private void forceNeighborsRecheck()
    {
        if (neighbors[NORTH] != null)
        {
            neighbors[NORTH].forceRecheck();
        }
        if (neighbors[EAST] != null)
        {
            neighbors[EAST].forceRecheck();
        }
        if (neighbors[SOUTH] != null)
        {
            //commented out to assume blocks to the south are connected elsewhere
            //neighbors[SOUTH].forceRecheck();
        }
        if (neighbors[WEST] != null)
        {
            neighbors[WEST].forceRecheck();
        }
    }

    private void removeFromMap()
    {
        BlockPathCheckResetter.blocks.Remove(this);
        if (neighbors[NORTH] != null)
        {
            neighbors[NORTH].neighbors[SOUTH] = null;
        }
        if (neighbors[EAST] != null)
        {
            neighbors[EAST].neighbors[WEST] = null;
        }
        if (neighbors[SOUTH] != null)
        {
            neighbors[SOUTH].neighbors[NORTH] = null;
        }
        if (neighbors[WEST] != null)
        {
            neighbors[WEST].neighbors[EAST] = null;
        }
    }

    private void addToMap()
    {
        BlockPathCheckResetter.blocks.Add(this);
        Block n;
        if(tryGetNeighbors(new Vector2(0, 1), out n)) //NORTH
        {
            neighbors[NORTH] = n;
            n.neighbors[SOUTH] = this;
        }
        if (tryGetNeighbors(new Vector2(1, 0), out n)) //EAST
        {
            neighbors[EAST] = n;
            n.neighbors[WEST] = this;
        }
        if (tryGetNeighbors(new Vector2(0, -1), out n)) //SOUTH
        {
            neighbors[SOUTH] = n;
            n.neighbors[NORTH] = this;
        }
        if (tryGetNeighbors(new Vector2(-1, 0), out n)) //WEST
        {
            neighbors[WEST] = n;
            n.neighbors[EAST] = this;
        }
    }

    private bool tryGetNeighbors(Vector2 direction, out Block neighbor)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(direction + (Vector2)transform.position, 0.25f);
        for(int i = 0; i < colliders.Length; i ++)
        {
            neighbor = colliders[i].GetComponent<Block>();
            if (neighbor != null && neighbor != this && !neighbor.isFalling)
            {
                return true;
            }
        }

        neighbor = null;
        return false;
    }

    //This should work ?
    static bool isGrounded(Block block, Block Sender = null)
    {
        if (block.pathedThisFrame)
        {
            return true;
        }
        if (block.neighbors[SOUTH] != null && block.neighbors[SOUTH] != Sender)
        {
            if (block.neighbors[SOUTH].isGround)
            {
                block.pathedThisFrame = true;
                return true;
            }
            if (isGrounded(block.neighbors[SOUTH], block)) 
            {
                block.pathedThisFrame = true;
                return true;
            }
        }

        if (block.neighbors[EAST] != null && block.neighbors[EAST] != Sender)
        {
            if (block.neighbors[EAST].isGround)
            {
                block.pathedThisFrame = true;
                return true;
            }
            if (isGrounded(block.neighbors[EAST], block))
            {
                block.pathedThisFrame = true;
                return true;
            }
        }

        if (block.neighbors[WEST] != null && block.neighbors[WEST] != Sender)
        {
            if (block.neighbors[WEST].isGround)
            {
                block.pathedThisFrame = true;
                return true;
            }
            if (isGrounded(block.neighbors[WEST], block))
            {
                block.pathedThisFrame = true;
                return true;
            }
        }
        return false;
    }
}
